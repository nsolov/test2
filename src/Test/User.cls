/// Description
Class Test.User Extends %Persistent
{
    Property Name as %String;

    Method Hello() as %Status
    {
        write "Hello " _ ..Name _"!"
        return $$$OK
    }

}